import 'package:e_4/models/control_model.dart';
import 'package:e_4/transaction.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:hive_flutter/adapters.dart';
import 'package:intl/intl.dart';

import 'boxes/boxes.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key, this.dData});
  final String? dData;

  @override
  State<HomePage> createState() => _HomePageState();

  test() {
    print("function called");
  }
}

class _HomePageState extends State<HomePage> {
  int totalIncome = 0;
  int totalExpense = 0;
  int totalBalance = 0;

  DateTime today = DateTime.now();
  DateTime now = DateTime.now();

  int index = 1;

  List<String> months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];
  @override
  void initState() {
    calIncome();
    calbalance();
    calExpense();

    super.initState();
  }

  void calIncome() {
    List<Dbcontrol> income = Boxes.getData()
        .values
        .cast<Dbcontrol>()
        .where((element) => element.type == 'Income')
        .toList();

    totalIncome = 0;
    for (Dbcontrol data in income) {
      setState(
        () {
          totalIncome = totalIncome + data.amount;
        },
      );
    }
  }

  void calbalance() {
    List<Dbcontrol> balance = Boxes.getData().values.toList();

    totalBalance = 0;
    for (Dbcontrol data in balance) {
      setState(
        () {
          totalBalance = totalBalance + data.amount;
        },
      );
    }
  }

  void calExpense() {
    List<Dbcontrol> income = Boxes.getData()
        .values
        .cast<Dbcontrol>()
        .where((element) => element.type == 'Expense')
        .toList();

    totalExpense = 0;
    for (Dbcontrol data in income) {
      setState(
        () {
          totalExpense = totalExpense + data.amount;
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffe2e7ef),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context)
              .push(
            CupertinoPageRoute(
              builder: (context) => const AddTransaction(),
            ),
          )
              .then((value) {
            calIncome();
            calbalance();
            calExpense();
          });
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            16.0,
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 155, 4, 4),
        child: const Icon(
          Icons.add_outlined,
          size: 32.0,
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(
              12.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "welcome ${widget.dData!}",
                  style: const TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.w700,
                    color: Color.fromARGB(255, 155, 4, 4),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(
              8.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                InkWell(
                  onTap: () {
                    setState(
                      () {
                        index = 3;
                      },
                    );
                  },
                  child: Container(
                    height: 50.0,
                    width: MediaQuery.of(context).size.width * 0.3,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        8.0,
                      ),
                      color: index == 3
                          ? const Color.fromARGB(255, 155, 4, 4)
                          : Colors.white,
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      months[now.month - 3],
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600,
                        color: index == 3
                            ? Colors.white
                            : const Color.fromARGB(255, 155, 4, 4),
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      index = 2;
                      today = DateTime(now.year, now.month - 1, today.day);
                    });
                  },
                  child: Container(
                    height: 50.0,
                    width: MediaQuery.of(context).size.width * 0.3,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        8.0,
                      ),
                      color: index == 2
                          ? const Color.fromARGB(255, 155, 4, 4)
                          : Colors.white,
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      months[now.month - 2],
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600,
                        color: index == 2
                            ? Colors.white
                            : const Color.fromARGB(255, 155, 4, 4),
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      index = 1;
                    });
                  },
                  child: Container(
                    height: 50.0,
                    width: MediaQuery.of(context).size.width * 0.3,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        8.0,
                      ),
                      color: index == 1
                          ? const Color.fromARGB(255, 155, 4, 4)
                          : Colors.white,
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      months[now.month - 1],
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600,
                        color: index == 1
                            ? Colors.white
                            : const Color.fromARGB(255, 155, 4, 4),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
            margin: const EdgeInsets.all(
              12.0,
            ),
            child: Ink(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(
                    24.0,
                  ),
                ),
              ),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(
                      24.0,
                    ),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: const Color.fromARGB(255, 244, 239, 239)
                          .withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: const Offset(0, 3),
                    ),
                  ],
                ),
                alignment: Alignment.center,
                padding: const EdgeInsets.symmetric(
                  vertical: 18.0,
                  horizontal: 8.0,
                ),
                child: Column(
                  children: [
                    const Text(
                      'Total Balance',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 22.0,
                          color: Color.fromARGB(255, 155, 4, 4)),
                    ),
                    const SizedBox(height: 12.0),
                    Text(
                      totalBalance.toString(),
                      style: TextStyle(
                        fontSize: 35.0,
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 155, 4, 4),
                      ),
                    ),
                    const SizedBox(
                      height: 12.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: const Color.fromARGB(255, 236, 168, 168),
                                borderRadius: BorderRadius.circular(
                                  20.0,
                                ),
                              ),
                              padding: const EdgeInsets.all(
                                6.0,
                              ),
                              margin: const EdgeInsets.only(
                                right: 8.0,
                              ),
                              child: const Icon(
                                Icons.arrow_downward,
                                size: 28.0,
                                color: Color.fromARGB(255, 11, 135, 17),
                              ),
                            ),
                            const Text(
                              "Income",
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Color.fromARGB(255, 155, 4, 4),
                              ),
                            ),
                            Text(
                              "+" + totalIncome.toString(),
                              style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.w700,
                                color: Color.fromARGB(255, 155, 4, 4),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: const Color.fromARGB(255, 236, 168, 168),
                                borderRadius: BorderRadius.circular(
                                  20.0,
                                ),
                              ),
                              padding: const EdgeInsets.all(
                                6.0,
                              ),
                              margin: const EdgeInsets.only(
                                right: 8.0,
                              ),
                              child: const Icon(
                                Icons.arrow_upward,
                                size: 28.0,
                                color: Color.fromARGB(255, 155, 4, 4),
                              ),
                            ),
                            Text(
                              'Expense',
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Color.fromARGB(255, 155, 4, 4),
                              ),
                            ),
                            Text(
                              totalExpense.toString(),
                              style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.w700,
                                color: Color.fromARGB(255, 155, 4, 4),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          const Text(
            " Transactions History",
            style: TextStyle(
              fontSize: 32.0,
              color: Color.fromARGB(255, 155, 4, 4),
              fontWeight: FontWeight.w900,
            ),
          ),
          Expanded(
            child: ValueListenableBuilder<Box<Dbcontrol>>(
              valueListenable: Boxes.getData().listenable(),
              builder: (context, box, _) {
                var data = box.values.toList().cast<Dbcontrol>();
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: ListView.builder(
                    itemCount: box.length,
                    reverse: true,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Card(
                          color: const Color.fromARGB(255, 236, 168, 168),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 15, horizontal: 10),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      data[index].amount.toString(),
                                      style: const TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold,
                                        color: Color.fromARGB(255, 155, 4, 4),
                                      ),
                                    ),
                                    const Spacer(),
                                    InkWell(
                                      onTap: () {
                                        delete(data[index]);
                                      },
                                      child: const Icon(
                                        Icons.delete,
                                        color: Color.fromARGB(255, 155, 4, 4),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 15,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                const HomePage(),
                                          ),
                                        ).then(
                                          (value) {
                                            setState(() {});
                                          },
                                        );
                                      },
                                      child: const Icon(
                                        Icons.edit,
                                        color: Color.fromARGB(255, 155, 4, 4),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const Spacer(),
                                    Text(
                                      DateFormat('MMMM d').format(
                                        DateTime.parse(
                                            data[index].date.toString()),
                                      ),
                                      style: const TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold,
                                        color: Color.fromARGB(255, 155, 4, 4),
                                      ),
                                    ),
                                    const Spacer(),
                                    Icon(
                                      data[index].type == "Expense"
                                          ? Icons.arrow_circle_down_outlined
                                          : Icons.arrow_circle_up_outlined,
                                      size: 28.0,
                                      color: data[index].type == "Expense"
                                          ? Colors.red
                                          : Colors.green,
                                    ),
                                    Text(
                                      data[index].type.toString(),
                                      style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Color.fromARGB(255, 155, 4, 4),
                                      ),
                                    ),
                                    const Spacer(),
                                    Text(
                                      data[index].des.toString(),
                                      style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Color.fromARGB(255, 155, 4, 4),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  void delete(Dbcontrol dbcontrol) async {
    await dbcontrol.delete();
    calIncome();
    calbalance();
    calExpense();
  }
}
