part of 'control_model.dart';

class DbcontrolAdapter extends TypeAdapter<Dbcontrol> {
  @override
  final int typeId = 0;

  @override
  Dbcontrol read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Dbcontrol(
      amount: fields[0] as int,
      type: fields[1] as String,
      date: fields[2] as DateTime,
      des: fields[3] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Dbcontrol obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.amount)
      ..writeByte(1)
      ..write(obj.type)
      ..writeByte(2)
      ..write(obj.date)
      ..writeByte(3)
      ..write(obj.des);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DbcontrolAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
