import 'package:hive/hive.dart';

part 'control_model.g.dart';

@HiveType(typeId: 0)
class Dbcontrol extends HiveObject {
  @HiveField(0)
  int amount;

  @HiveField(1)
  String type;

  @HiveField(2)
  DateTime date;

  @HiveField(3)
  String des;

  Dbcontrol({
    required this.amount,
    required this.type,
    required this.date,
    required this.des,
  });
}
